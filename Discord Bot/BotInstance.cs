using System;
using System.Linq;
using System.Threading.Tasks;
using DSharpPlus;
using DSharpPlus.Entities;
using DSharpPlus.EventArgs;
using DSharpPlus.Interactivity;
using DSharpPlus.Interactivity.Extensions;
using DSharpPlus.SlashCommands;
using DSharpPlus.SlashCommands.EventArgs;
using Microsoft.Extensions.Logging;

namespace Bot;

class BotInstance
{
    private readonly ILogger<BotInstance> _logger;
    private readonly DiscordClient _client;
    private readonly Json.Server _server;

    public BotInstance(string token, ILoggerFactory loggerFactory, Json.Server server)
    {
        _logger = loggerFactory.CreateLogger<BotInstance>();
        _server = server;
        _logger.LogInformation("Hello from Bot Instance!");
        
        var config = new DiscordConfiguration
        {
            Token = token,
            TokenType = TokenType.Bot,
            AutoReconnect = true,
            Intents = DiscordIntents.GuildMembers |
                      DiscordIntents.DirectMessages |
                      DiscordIntents.GuildMessages |
                      DiscordIntents.Guilds |
                      DiscordIntents.MessageContents,
            LoggerFactory = loggerFactory,
            HttpTimeout = TimeSpan.FromMinutes(10),
            MinimumLogLevel = LogLevel.Trace
        };
        
        _client = new DiscordClient(config);
        _client.Ready += (client, _) =>
        {
            _logger.LogInformation("{UserName} is ready", client.CurrentUser.Username);
            return Task.CompletedTask;
        };
        
        var extension = _client.UseSlashCommands();
        extension.RegisterCommands<SlashCommands>();
        extension.SlashCommandErrored += OnSlashCommandErrored;
        extension.SlashCommandInvoked += OnSlashCommandInvoked;
        
        _client.MessageCreated += OnMessageCreated;
        
        _client.UseInteractivity(new InteractivityConfiguration
        {
            Timeout = TimeSpan.FromMinutes(2)
        });
    }

    private async Task OnMessageCreated(DiscordClient sender, MessageCreateEventArgs args)
    {
        if (args.Guild == null || !args.Guild.Id.Equals(_server.ID) || args.Author.Id.Equals(_client.CurrentUser.Id))
        {
            return;
        }
        
        await CheckForUpdatesMessage(args);
        await CheckForOldDiagnosticsZip(args);
    }

    private Task OnSlashCommandInvoked(SlashCommandsExtension sender, SlashCommandInvokedEventArgs args)
    {
        var member = args.Context.Member;
        _logger.LogInformation("{DisplayName}({ID}) Invoked a slash command", member.DisplayName, member.Id);
        return Task.CompletedTask;
    }

    private Task OnSlashCommandErrored(SlashCommandsExtension sender, SlashCommandErrorEventArgs args)
    { 
        _logger.LogError(args.Exception, "Failed to run Slash Command");
        return Task.CompletedTask;
    }

    public Task RunAsync()
    {
        _logger.LogInformation("Connecting...");
        return _client.ConnectAsync();
    }

    private async Task CheckForUpdatesMessage(MessageCreateEventArgs args)
    {
        if (!args.Channel.Id.Equals(_server.UpdatesChannel.ID))
        {
            return;
        }

        _logger.LogInformation(
            "Message {MessageId} from guid {GuildName}({GuildId}) is in the updates channel", 
            args.Message.Id,
            args.Guild.Name,
            args.Guild.Id);
        
        var message = args.Message.Content;

        const string steamUrlStart = "https://steamcommunity.com/sharedfiles/filedetails/";
        if (string.IsNullOrEmpty(message) || !message.Contains(steamUrlStart))
        {
            var member = await args.Guild.GetMemberAsync(args.Author.Id);
            await RemoveUsersMessage(args.Message, member);
        }
    }

    private async Task RemoveUsersMessage(DiscordMessage message, DiscordMember member)
    {
        var rawText = message.Content;
        var messageTemplate = _server.UpdatesChannel.MessageTemplateForDeletion;
        
        var messageToSend = messageTemplate.Replace("{Message}", $"```{rawText}```");
        
        _logger.LogInformation("Deleting invalid message");
        await message.DeleteAsync("Message does not have steam url present");

        var directMessageChannel = await member.CreateDmChannelAsync();
        await directMessageChannel.SendMessageAsync(messageToSend);
    }

    private async Task CheckForOldDiagnosticsZip(MessageCreateEventArgs args)
    {
        if (string.IsNullOrEmpty(_server.DiagnosticsZipDetectedMessage) || 
            !args.Message.Attachments.Any(attachment => attachment.FileName.StartsWith("DiagnosticsZip_") && attachment.FileName.EndsWith(".zip")))
        {
            return;
        }

        var message = new DiscordMessageBuilder()
            .WithContent(_server.DiagnosticsZipDetectedMessage)
            .WithReply(args.Message.Id, true)
            .WithAllowedMention(new RepliedUserMention());

        await args.Channel.SendMessageAsync(message);
    }
}