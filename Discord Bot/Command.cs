﻿using System;
using DSharpPlus;
using DSharpPlus.EventArgs;

namespace Bot;

class Command
{
    public readonly string Quote;
    public readonly Action<DiscordClient, MessageCreateEventArgs> Method;
    public readonly string Description;
    public readonly bool Hidden;

    public Command(string quote, Action<DiscordClient, MessageCreateEventArgs> method, string description,
        bool hidden = false)
    {
        Quote = quote;
        Method = method;
        Description = description;
        Hidden = hidden;
    }
}