﻿namespace Bot.Json;

public class BotSettings
{
    public string Token { get; set; }
    public Server[] Servers { get; set; }
}