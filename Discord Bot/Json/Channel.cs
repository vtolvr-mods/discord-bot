namespace Bot.Json;

public class Channel
{
    public ulong ID { get; set; }
    public string MessageTemplateForDeletion { get; set; }
}