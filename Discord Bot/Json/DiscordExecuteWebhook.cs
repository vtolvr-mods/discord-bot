namespace Bot.Json;

public class DiscordExecuteWebhook
{
    public string Content { get; set; }
    public string UserName { get; set; }
}