namespace Bot.Json;

public class Server
{
    public ulong ID { get; set; }
    public Channel UpdatesChannel { get; set; }
    public string DiagnosticsZipDetectedMessage { get; set; }
}