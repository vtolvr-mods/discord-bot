using System;
using System.IO;
using System.Linq;
using JNogueira.Logger.Discord;
using System.Threading;
using System.Threading.Tasks;
using Azure.Storage.Blobs;
using Bot.Json;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace Bot;

internal class Program
{
    public static DateTime StarTime = DateTime.UtcNow;
    private const string botDataFileName = "BotData.json";
    private static ILogger _logger;
    private static ILoggerFactory _factory;

    private static async Task Main()
    {
        var services = new ServiceCollection();
        services.AddLogging(builder =>
        {
            builder.AddConsole();
            builder.SetMinimumLevel(LogLevel.Trace);
        });   
        var provider = services.BuildServiceProvider();

        _factory = provider.GetService<ILoggerFactory>();
        var startUpLogger = _factory.CreateLogger("StartUp");
        
        var settings = await LoadDataAsync(startUpLogger);
        if (settings == null)
        {
            if (File.Exists(botDataFileName))
            {
                startUpLogger.LogWarning("The bot data file existed but settings is null. Deleting the file");
                File.Delete(botDataFileName);
            }
            startUpLogger.LogWarning("Bye");
            return;
        }
        
        _logger = _factory.CreateLogger<Program>();
        _logger.LogInformation("Starting at {DateTimeNowUTC}", StarTime);
        
        var currentThread = Thread.CurrentThread;
        currentThread.Name = "Main Thread";
        new Thread(() => CreateHealthPointProbe(currentThread, _factory, settings)).Start();

        await StartBot(_factory, settings);
    }

    private static async Task StartBot(ILoggerFactory factory, BotSettings settings)
    {
        try
        {
            var botInstance = new BotInstance(settings.Token, factory, settings.Servers.First());
            await botInstance.RunAsync();
        }
        catch (Exception e)
        {
            _logger.LogError(e, "Failed to run bot");
            throw;
        }
        await Task.Delay(-1);
        _logger.LogWarning("'Task.Delay(-1)' has finished some how");
    }

    private static async Task<BotSettings> LoadDataAsync(ILogger logger)
    {
        logger.LogInformation("Loading data");
        
        if (!File.Exists(botDataFileName))
        {
            logger.LogWarning("{ExpectedConfigFileName} did not exist in current working directory.", botDataFileName);
        }
        
        try
        {
            string text;
            if (!string.IsNullOrEmpty(Environment.GetEnvironmentVariable("Azure")))
            {
                logger.LogInformation("Azure environment variable detected. Downloading bot data");
                text = await DownloadDataFile();
            }
            else
            {
                text = await File.ReadAllTextAsync(botDataFileName);
            }
            
            logger.LogInformation("Finished reading {ConfigFileName}", Path.Combine(Directory.GetCurrentDirectory(), botDataFileName));
            return JsonConvert.DeserializeObject<BotSettings>(text);
        }
        catch (Exception e)
        {
            logger.LogError(e, "Failed to load {ConfigFileName}", botDataFileName);
            return null;
        }
    }

    private static async Task<string> DownloadDataFile()
    {
        var connectionString = Environment.GetEnvironmentVariable("StorageAccountConnectionString");
        var containerName = Environment.GetEnvironmentVariable("StorageContainerName");

        if (string.IsNullOrEmpty(connectionString))
        {
            _logger.LogError("The connection string is not set in the environment variables");
        }

        if (string.IsNullOrEmpty(containerName))
        {
            _logger.LogError("The container name is not set in the environment variables");
        }
        
        var blobServiceClient = new BlobServiceClient(connectionString);
        var container = blobServiceClient.GetBlobContainerClient(containerName);
        var blob = container.GetBlobClient(botDataFileName);
        var content = await blob.DownloadContentAsync();
        return content.Value.Content.ToString();
    }

    /// <summary>
    /// This method is just to create a endpoint at 8080 to please Azure App Service
    /// </summary>
    private static void CreateHealthPointProbe(Thread threadToMonitor, ILoggerFactory factory, BotSettings settings)
    {
        var builder = WebApplication.CreateBuilder();

        // Add services to the container.
        // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
        builder.Services.AddEndpointsApiExplorer();
        builder.Services.AddSwaggerGen();

        var app = builder.Build();

        // Configure the HTTP request pipeline.
        if (app.Environment.IsDevelopment())
        {
            app.UseSwagger();
            app.UseSwaggerUI();
        }
        app.UseHttpsRedirection();
        var threadCount = 1;
        app.MapGet("/", () =>
            {
                if (!threadToMonitor.IsAlive)
                {
                    _logger.LogWarning("Thread state is {ThreadState}", Enum.GetName(threadToMonitor.ThreadState));
                    // threadToMonitor = new Thread(async () => await StartBot(factory, settings))
                    // {
                    //     Name = $"Thread {threadCount++}"
                    // };
                    // threadToMonitor.Start();
                    // _logger.LogWarning("The thread was unhealthy, it has been started back up again. {ThreadName}", threadToMonitor.Name);
                    // return $"The thread was unhealthy, it has been started back up again. {threadToMonitor.Name}";
                }

                return threadToMonitor.IsAlive
                    ? $"{threadToMonitor.Name} is healthy"
                    : $"{threadToMonitor.Name} is unhealthy. {Enum.GetName(threadToMonitor.ThreadState)}";
            })
            .WithName("GetHealth")
            .WithOpenApi();
        app.Run();
    }
}