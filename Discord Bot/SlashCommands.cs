using System;
using System.Text;
using System.Threading.Tasks;
using DSharpPlus;
using DSharpPlus.Entities;
using DSharpPlus.SlashCommands;
using DSharpPlus.SlashCommands.Attributes;

namespace Bot;

public class SlashCommands : ApplicationCommandModule
{
    [SlashCommand(nameof(ThisIsEpic), "Nebriv said it was epic")]
    public async Task ThisIsEpic(InteractionContext context)
    {
        await context.CreateResponseAsync("https://i.imgur.com/maDH4P0.png");
    }
    
    [SlashCommand(nameof(Source), "Posts the URL to the source code of the bot")]
    public async Task Source(InteractionContext context)
    {
        await context.CreateResponseAsync(
            "You can view my source code [on GitLab](https://gitlab.com/vtolvr-mods/discord-bot)");
    }
    
    [SlashCommand(nameof(Stats), "Displays how many members are in this guild")]
    public async Task Stats(InteractionContext ctx)
    {
        var message = new StringBuilder($"Stats for {ctx.Guild.Name}\n");
        var uptime = DateTime.UtcNow.Subtract(Program.StarTime);
        message.AppendLine($"Uptime = {uptime.Days} days {uptime.Hours} hours {uptime.Minutes} minutes");
        message.AppendLine($"{ctx.Guild.Name} has {ctx.Guild.MemberCount} members!");
            
        var roles = ctx.Guild.Roles;
        if (roles.Count != 0)
        {
            message.AppendLine($"{ctx.Guild.Name} has {roles.Count} {(roles.Count == 1 ? "role" : "roles")}");
        }
        await ctx.CreateResponseAsync(message.ToString());
    }
    
    [SlashCommand(nameof(Ping), "Display's the bots ping to Discord's API")]
    [SlashRequirePermissions(Permissions.ManageGuild)]
    public async Task Ping(InteractionContext ctx)
    {
        await ctx.CreateResponseAsync($"Pong!\nPing: {ctx.Client.Ping}ms");
    }
    
    [SlashCommand(nameof(Say), "Says a message from the bot in a given channel")]
    [SlashRequirePermissions(Permissions.Administrator)]
    public async Task Say(InteractionContext ctx,
        [Option("Channel", "The channel to send the message in")] DiscordChannel channel,
        [Option("Message", "The message you want the bot to say")] string message)
    {
        await ctx.CreateResponseAsync(InteractionResponseType.DeferredChannelMessageWithSource);
        var builder = new DiscordWebhookBuilder();
        switch (channel.Type)
        {
            case ChannelType.Category:
            case ChannelType.Stage:
            case ChannelType.Voice:
                builder.WithContent($"The channel can't be in type \"{channel.Type}\"");
                break;
            default:
                var discordMessage = await ctx.Client.SendMessageAsync(channel, message);
                builder.AddEmbed(new DiscordEmbedBuilder
                {
                    Title = $"Message Sent in {channel.Name}",
                    Description = discordMessage.JumpLink.ToString()
                });
                break;
        }

        await ctx.EditResponseAsync(builder);
    }
}