# VTOL VR Modding Server's Discord Bot
[![Discord](https://img.shields.io/discord/597153468834119710?label=VTOL%20VR%20Modding&logo=discord&style=flat-square)](https://discord.gg/XZeeafp "Discord Invite")

This is the repository for the discord bot which runs in the VTOL VR Modding discord server. The bot has been rewritten in C# using [DSharpPlus](https://github.com/DSharpPlus/DSharpPlus)

## Installation 

You'll need .Net Core 3.1 installed and the packages from the NuGet package manager. Create a BotData.json with the exe and fill it out like the example below.

BotData.json Example:

    {
      "Token": "TOKEN"
    }

## [Contributions](https://gitlab.com/vtolvr-mods/discord-bot/-/graphs/master, "Contributors")

Ketkev added a responce when a user links the multiplayer mod's github repository. (Old Python Bot & Removed)

Temperz87 added a counter for how many people said "multiplayer" (Old Python Bot & Removed)

Contributions are welcomed, if you would like to add a feature to the bot to be added to the official discord, fork the project, add your features then create a pull request and if it gets approved it will be added into the bot.


## Useful links

- [VTOL VR Modding Discord Server](https://discord.gg/XZeeafp "https://discord.gg/XZeeafp")
- [DSharpPlus's Documentation]("https://dsharpplus.emzi0767.com/api/index.html")
- [DSharpPlus's Github Repository](https://github.com/DSharpPlus/DSharpPlus")